# adcwebtool

Teensy ADC SerialCommand Arduino firmware interfaced to GraphJS visualization using Python and websockets

## firmware

Install dependencies below.  Configure Arduino for your Teensy board.  Compile and upload `teensy-firmware/adc_scmd/adc_scmd.ino`.  Try running test script: 
```bash
python teensy-firmware/adc_scmd/test_acquire.py
```

### Teensyduino

Install arduino.cc distro v1.8.13 (most recent that works with Teensyduino 1.53).

Install [teensyduino 1.53](https://www.pjrc.com/teensy/td_download.html).  On Linux, follow instruction for udev rules.

Select the folder to `arduino-1.8.13` and proceed.

Check the box to install the ADC library.

### SerialCommand

Clone project https://github.com/p-v-o-s/Arduino-SerialCommand.git as `SerialCommand` in `Arduino/libraries` path.

