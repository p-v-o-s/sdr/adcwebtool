import struct
from collections import OrderedDict
import serial
import numpy as np

#import IPython;IPython.embed() # USE ME FOR DEBUGGING

################################################################################
PORT = '/dev/ttyACM0'
BAUD = 115200
DEBUG = True

class ScmdComm:
    def __init__(self, port=PORT):
        self.ser = serial.Serial(PORT,BAUD,timeout=1)
    
    def send(self, cmd, endl='\n'):
        if DEBUG:
            print(f"-> {cmd}")
        self.ser.write(bytes(cmd+endl,'utf8'))

    def readline(self):
        return self.ser.readline()


################################################################################
CHAN0_PIN = 2
CHAN1_PIN = 3
RES = 12
AVGS = 8
CONV_SPEED = 4
SAMP_SPEED = 4

class ADC_Driver(ScmdComm):
    def config(self,
               chan0_pin = CHAN0_PIN,
               chan1_pin = CHAN1_PIN,
               resolution = RES,
               averages   = AVGS,
               conv_speed = CONV_SPEED,
               samp_speed = SAMP_SPEED,
              ):
        self.send(f"ADC.SET_READ_PIN {chan0_pin} 0")
        self.send(f"ADC.SET_READ_PIN {chan1_pin} 1")
        self.send(f"ADC.SET_RES {resolution}")
        self.send(f"ADC.SET_AVG {averages}")
        self.send(f"ADC.SET_CONV_SPEED {conv_speed}")
        self.send(f"ADC.SET_SAMP_SPEED {samp_speed}")

    def start_acquisition(self):
        self.ser.flush()
        self.send("ADC.START")

    def stop_acquisition(self):
        self.send("ADC.STOP")
        self.ser.flush()


        
    def sync_packet(self, decode_buffer=True):
        while True:
            line = self.readline().strip()
            line = line.strip()
            if line.startswith(b"BUF"):
                line = line[3:].rstrip(b':') #strip off the buffer header tag and end marker
                #split header into parts
                adc_chan, seq_num, buffer_size, dma_end_micros = map(int,line.split(b','))
                buffer_size = int(buffer_size)
                #read binary data
                data = self.ser.read(buffer_size*2); # uint16_t elements
                #read newline
                line = self.readline()
                assert(line == b'\n')
                #read footer
                line = self.readline().strip()
                assert(line.startswith(b"END_BUF"))
                line = line[7:]#strip off the tag
                #split footer into parts
                adc_chan, serial_end_micros = map(int,line.split(b','))
                if decode_buffer:
                    data = struct.unpack(f"<{buffer_size}H", data)
                return {'adc_chan': adc_chan,
                        'seq_num':  seq_num,
                        'buffer_size': buffer_size,
                        'dma_end_micros': dma_end_micros,
                        'serial_end_micros': serial_end_micros,
                        'data': data
                       }
            else:
                print(f"WARNING: sync_buffer is discarding line: {line}")
################################################################################

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    OUTFILE_NAME = 'out.csv'
    OUTFILE_COLS = ['X_micros','V0','V1']
    OUTFILE_FMT  = '%0.3f,%i,%i'
    
    ADC = ADC_Driver()

    timestamps   = []
    data         = []

    try:
        ADC.start_acquisition()
        while True:
            pkt = ADC.sync_packet()
            ts = pkt['dma_end_micros']/1e6
            timestamps.append(ts)
            data.append(pkt['data'])
    except KeyboardInterrupt:
        pass
    finally:
        ADC.stop_acquisition()

    timestamps = np.array(timestamps)
    #concatenate all buffers and convert to array
    data = np.array([samp for buff in data for samp in buff])

    #TODO figure out how to create sample level timestamps from te buffer level ones
    
    #plot the data
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(data,'.')
    ax.set_xlabel("Time [s]")
    ax.set_title(f"ADC test")
    plt.show()
    
