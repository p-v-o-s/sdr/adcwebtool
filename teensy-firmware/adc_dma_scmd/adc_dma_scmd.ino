/* Example for using DMA with ADC
    This example uses DMA object to do the sampling.  It does not use a timer so it runs
    at whatever speed the ADC will run at with current settings.

    It should work for Teensy LC, 3.x and T4

    DMA: using AnalogBufferDMA with two buffers, this runs in continuous mode and when one buffer fills
    an interrupt is signaled, which sets flag saying it has data, which this test application
    scans the data, and computes things like a minimum, maximum, average values and an RMS value.
    For the RMS it keeps the average from the previous set of data.
*/

//teensyduino standard
#include <stdint.h>

//teensyduino community
#include <ADC.h>
#include <AnalogBufferDMA.h>

//third party PVOS!
#include <SerialCommand.h>  /* https://github.com/p-v-o-s/Arduino-SerialCommand */

////////////////////////////////////////////////////////////////////////////////
#ifdef ADC_USE_DMA

#define IDN_STRING "PVOS ADC Device"
#define DEBUG_PORT Serial

/******************************************************************************/
// CPU Frequency selection
//   ref: https://forum.pjrc.com/threads/57196-T4-heatsink-recommended?p=215941&viewfull=1#post215941
#if 1
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__IMXRT1062__)
uint32_t set_arm_clock(uint32_t frequency);
#endif
#ifdef __cplusplus
} // extern "C"
#endif
#endif
/******************************************************************************/
// SerialCommand parser
const unsigned int SERIAL_BAUDRATE = 115200;
SerialCommand sCmd_USB(Serial, 20); // (Stream, int maxCommands)

/******************************************************************************/
// ADC subsystem

// This version uses both ADC1 and ADC2
enum adc_config_mode_t {ADC_CONFIG_CHAN0,ADC_CONFIG_CHAN1,ADC_CONFIG_SYNC};
adc_config_mode_t adc_config_mode = ADC_CONFIG_SYNC;
bool adc_chan0_is_configured = false;
bool adc_chan1_is_configured = false;
int  adc_num_active = 0;
#if defined(KINETISL)
int adc0_readPin = A0;
#elif defined(KINETISK)
int adc0_readPin = A0;
int adc1_readPin = A2;
#else
int adc0_readPin = A0;
int adc1_readPin = 26;
#endif

ADC *adc = new ADC(); // adc object

// Going to try two buffers here  using 2 dmaSettings and a DMAChannel
#ifdef KINETISL
const uint32_t DMA_BUFFER_SIZE = 500;
#else
const uint32_t DMA_BUFFER_SIZE = 1600;
#endif

DMAMEM static volatile uint16_t __attribute__((aligned(32))) dma_adc_buff1[DMA_BUFFER_SIZE];
DMAMEM static volatile uint16_t __attribute__((aligned(32))) dma_adc_buff2[DMA_BUFFER_SIZE];
AnalogBufferDMA abdma1(dma_adc_buff1, DMA_BUFFER_SIZE, dma_adc_buff2, DMA_BUFFER_SIZE);

#ifdef ADC_DUAL_ADCS
DMAMEM static volatile uint16_t __attribute__((aligned(32))) dma_adc2_buff1[DMA_BUFFER_SIZE];
DMAMEM static volatile uint16_t __attribute__((aligned(32))) dma_adc2_buff2[DMA_BUFFER_SIZE];
AnalogBufferDMA abdma2(dma_adc2_buff1, DMA_BUFFER_SIZE, dma_adc2_buff2, DMA_BUFFER_SIZE);
#endif

void adc_set_defaults(void){
    adc_config_mode = ADC_CONFIG_SYNC;
    adc_chan0_is_configured = false;
    adc_chan1_is_configured = false;
    adc_num_active = 0;
    #if defined(KINETISL)
    int adc0_readPin = A0;
    #elif defined(KINETISK)
    int adc0_readPin = A0;
    int adc1_readPin = A2;
    #else
    int adc0_readPin = A0;
    int adc1_readPin = 26;
    #endif
}

void adc_dma_start_continuous(){
        // Set ADC defaults
    // reference can be ADC_REFERENCE::REF_3V3, ADC_REFERENCE::REF_1V2 (not for Teensy LC) or ADC_REF_EXT.
    //adc->setReference(ADC_REFERENCE::REF_1V2, ADC_0); // change all 3.3 to 1.2 if you change the reference to 1V2
    // always call the compare functions after changing the resolution!
    //adc->enableCompare(1.0/3.3*adc->getMaxValue(ADC_0), 0, ADC_0); // measurement will be ready if value < 1.0V
    //adc->enableCompareRange(1.0*adc->getMaxValue(ADC_1)/3.3, 2.0*adc->getMaxValue(ADC_1)/3.3, 0, 1, ADC_1); // ready if value lies out of [1.0,2.0] V
    // enable DMA and interrupts
    Serial.flush();
    // setup a DMA Channel.
    abdma1.init(adc, ADC_0);
    adc_num_active = 1;
    #ifdef ADC_DUAL_ADCS
    abdma2.init(adc, ADC_1);
    adc->adc1->startContinuous(adc1_readPin);
    adc_num_active = 2;
    #endif
    // Start the dma operation..
    adc->adc0->startContinuous(adc0_readPin);
}


/******************************************************************************/
// SETUP

elapsedMicros since_setup_started_micros;

void setup() {
    since_setup_started_micros = 0;
    //==========================================================================
    // Setup SerialCommand for USB interface
    //--------------------------------------------------------------------------
    sCmd_USB.setDefaultHandler(UNRECOGNIZED_sCmd_default_handler);
    sCmd_USB.addCommand("IDN?",          IDN_sCmd_query_handler);
    sCmd_USB.addCommand("ADC.CONFIG",         ADC_CONFIG_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.SET_READ_PIN",   ADC_SET_READ_PIN_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.SET_SAMP_SPEED", ADC_SET_SAMP_SPEED_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.SET_CONV_SPEED", ADC_SET_CONV_SPEED_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.SET_RES",        ADC_SET_RES_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.SET_AVG",        ADC_SET_AVG_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.START",          ADC_START_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.STOP",           ADC_STOP_sCmd_action_handler);
    sCmd_USB.addCommand("ADC.RESET",          ADC_RESET_sCmd_action_handler);
    sCmd_USB.addCommand("T",                  TEMPMON_GET_TEMP_sCmd_query_handler);
    sCmd_USB.addCommand("TEMPMON.GET_TEMP?",  TEMPMON_GET_TEMP_sCmd_query_handler);
    //--------------------------------------------------------------------------
    // intialize the serial device
    while (!Serial && millis() < 1000) ; //wait for USB serial ready
    Serial.begin(SERIAL_BAUDRATE);  //USB serial on the Teensy 3/4
    //--------------------------------------------------------------------------
    // initialize pins
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(adc0_readPin, INPUT);
    pinMode(adc1_readPin, INPUT); //pin 23 single ended
}

/******************************************************************************/
// Mainloop
volatile bool mainloop_is_idle = true;

void loop() {
    mainloop_is_idle = true; //assume we will be idle until we do something
    //--------------------------------------------------------------------------
    // process Serial commands over USB
    size_t num_bytes = sCmd_USB.readSerial();
    if (num_bytes > 0){
        mainloop_is_idle = false;
        // CRITICAL SECTION --------------------------------------------------------
        //unsigned char sreg_backup = SREG;   /* save interrupt enable/disable state */
        cli();
        sCmd_USB.processCommand();
        sei();
        //SREG = sreg_backup;                 /* restore interrupt state */
        // END CRITICAL SECTION ----------------------------------------------------
    }
    //--------------------------------------------------------------------------
    // Handle ADC data
    if (adc_num_active > 0){
        mainloop_is_idle = false; //make sure our response is quick!
        // Maybe only when both have triggered?
        // #ifdef ADC_DUAL_ADCS
        //     if ( abdma1.interrupted() && abdma2.interrupted()) {
        //         if (abdma1.interrupted()) ProcessAnalogData(&abdma1, 0);
        //         if (abdma2.interrupted()) ProcessAnalogData(&abdma2, 1);
        //     }
        // #else
            if ( abdma1.interrupted()) {
                ProcessAnalogData(&abdma1, 0);
            }
        // #endif
    }
    
    //--------------------------------------------------------------------------
    // rest a bit if doing nothing
    if (mainloop_is_idle){
        //decrease CPU speed
        set_arm_clock(24000000); //24MHz
        delay(10);}
}

void ProcessAnalogData(AnalogBufferDMA *pabdma, int8_t adc_num) {
    int buffer_size = pabdma->bufferCountLastISRFilled();
    volatile uint16_t *pbuffer     = pabdma->bufferLastISRFilled();
    volatile uint16_t *end_pbuffer = pbuffer + buffer_size;
    // Delete data from the cache, without touching memory
    if ((uint32_t)pbuffer >= 0x20200000u)  arm_dcache_delete((void*)pbuffer, sizeof(dma_adc_buff1));
    //print buffer packet header
    Serial.printf("BUF%d,%u,%u,%u:\n", 
                  adc_num,
                  pabdma->interruptCount(),
                  buffer_size,
                  pabdma->interruptTimeMicros()
                  );
    
    //write out the buffer
    Serial.write((byte*) pbuffer, buffer_size*sizeof(uint16_t));
 
    //send the footer
    Serial.printf("\nEND_BUF%d,%u\n", 
                  adc_num,
                  pabdma->sinceInitMicros()
                  );
    //allow DMA to continue
    pabdma->clearInterrupt();
}

/******************************************************************************/
// SerialCommand Handlers

void IDN_sCmd_query_handler(SerialCommand this_sCmd){
    this_sCmd.println(F(IDN_STRING));
}


void ADC_CONFIG_sCmd_action_handler(SerialCommand this_sCmd){
    // 0: channel 0, 1: channel 1, 2: synchronized
    int level;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.CONFIG requires 1 argument 'mode' = {0:CHAN0,1:CHAN1,2:SYNC}\n"));
    }
    else{
        level = atoi(arg);
        switch(level){
            case 0: adc_config_mode = ADC_CONFIG_CHAN0;
                    adc_chan0_is_configured = true;
                    break;
            case 1: adc_config_mode = ADC_CONFIG_CHAN1;
                    adc_chan1_is_configured = true;
                    break;
            case 2: adc_config_mode = ADC_CONFIG_SYNC;
                    break;
            default:{
                this_sCmd.print(F("#ERROR: ADC.CONFIG requires 1 argument 'mode' = {0:CHAN0,1:CHAN1,2:SYNC}\n"));
                break;
            }
        }
    }
}

void ADC_SET_READ_PIN_sCmd_action_handler(SerialCommand this_sCmd){
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.SET_READ_PIN requires 1 argument 'Apin'= {0,1,2,3,4,5,6,7,8,9,10,11,12,13}\n"));
    }
    else{
        int Apin = atoi(arg);
        int pin;
        switch(Apin){
            case 0: pin = A0; break;
            case 1: pin = A1; break;
            case 2: pin = A2; break;
            case 3: pin = A3; break;
            case 4: pin = A4; break;
            case 5: pin = A5; break;
            case 6: pin = A6; break;
            case 7: pin = A7; break;
            case 8: pin = A8; break;
            case 9: pin = A9; break;
            case 10: pin = A10; break;
            case 11: pin = A11; break;
            case 12: pin = A12; break;
            case 13: pin = A13; break;
            default:{
                this_sCmd.print(F("#ERROR: ADC.SET_READ_PIN requires 1 argument 'Apin'= {0,1,2,3,4,5,6,7,8,9,10,11,12,13}\n"));
                return;
            }
        }
        //set the param depending on the config mode
        if (adc_config_mode == ADC_CONFIG_CHAN0){
            adc0_readPin = pin;
            return;
        } else if (adc_config_mode == ADC_CONFIG_CHAN1){
            adc1_readPin = pin;
            return;
        } else if (adc_config_mode == ADC_CONFIG_SYNC){
            //must specify the channel next
            arg = this_sCmd.next();
            if (arg != NULL){
                int chan = atoi(arg);
                if(chan == 0){
                    adc0_readPin = pin;
                    return;
                } else if (chan == 1){
                    adc1_readPin = pin;
                    return;
                }
            }
            //failed
            this_sCmd.print(F("#ERROR: ADC.SET_READ_PIN requires 2nd argument 'chan'= {0,1} when ADC_CONFIG_SYNC mode is set.\n"));
        }
    }
}

void ADC_SET_SAMP_SPEED_sCmd_action_handler(SerialCommand this_sCmd){
    //ADC_SAMPLING_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED, HIGH_SPEED or VERY_HIGH_SPEED
    int level;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.SET_SAMP_SPEED requires 1 argument 'level'= {0,1,2,3,4}\n"));
    }
    else{
        level = atoi(arg);
        ADC_SAMPLING_SPEED speed;
        switch(level){
            case 0: speed = ADC_SAMPLING_SPEED::VERY_LOW_SPEED;  break;
            case 1: speed = ADC_SAMPLING_SPEED::LOW_SPEED;       break;
            case 2: speed = ADC_SAMPLING_SPEED::MED_SPEED;       break;
            case 3: speed = ADC_SAMPLING_SPEED::HIGH_SPEED;      break;
            case 4: speed = ADC_SAMPLING_SPEED::VERY_HIGH_SPEED; break;
            default:{
                this_sCmd.print(F("#ERROR: ADC.SET_SAMP_SPEED requires 1 argument 'level'= {0,1,2,3,4}\n"));
                return;
            }
        }
        //set the param depending on the config mode
        if (adc_config_mode == ADC_CONFIG_CHAN0 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc0->setSamplingSpeed(speed);
        }
        if (adc_config_mode == ADC_CONFIG_CHAN1 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc1->setSamplingSpeed(speed);
        }
    }
}

void ADC_SET_CONV_SPEED_sCmd_action_handler(SerialCommand this_sCmd){
    // ADC_CONVERSION_SPEED enum: VERY_LOW_SPEED, LOW_SPEED, MED_SPEED,
    // HIGH_SPEED_16BITS, HIGH_SPEED, VERY_HIGH_SPEED
    int level;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.SET_CONV_SPEED requires 1 argument 'level'= {0,1,2,3,4}\n"));
    }
    else{
        level = atoi(arg);
        ADC_CONVERSION_SPEED speed;
        switch(level){
            case 0: speed = ADC_CONVERSION_SPEED::VERY_LOW_SPEED;  break;
            case 1: speed = ADC_CONVERSION_SPEED::LOW_SPEED;       break;
            case 2: speed = ADC_CONVERSION_SPEED::MED_SPEED;       break;
            case 3: speed = ADC_CONVERSION_SPEED::HIGH_SPEED;      break;
            case 4: speed = ADC_CONVERSION_SPEED::VERY_HIGH_SPEED; break;
            default:{
                this_sCmd.print(F("#ERROR: ADC.SET_CONV_SPEED requires 1 argument 'level'= {0,1,2,3,4}\n"));
                return;
            }
        }
        //set the param depending on the config mode
        if (adc_config_mode == ADC_CONFIG_CHAN0 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc0->setConversionSpeed(speed);
        }
        if (adc_config_mode == ADC_CONFIG_CHAN1 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc1->setConversionSpeed(speed);
        }
    }
}

void ADC_SET_RES_sCmd_action_handler(SerialCommand this_sCmd){
    int bits;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.SET_RES requires 1 argument 'bits'= {8,10,12,16}\n"));
    }
    else{
        bits = atoi(arg);
        switch(bits){
            case 8:
            case 10:
            case 12:
            case 16: break;
            default:{
                this_sCmd.print(F("#ERROR: ADC.SET_RES requires 1 argument 'bits'= {8,10,12,16}\n"));
                return;
            }
        }
        //set the param depending on the config mode
        if (adc_config_mode == ADC_CONFIG_CHAN0 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc0->setResolution(bits);
        }
        if (adc_config_mode == ADC_CONFIG_CHAN1 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc1->setResolution(bits);
        }
    }
}

void ADC_SET_AVG_sCmd_action_handler(SerialCommand this_sCmd){
    int averages;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        this_sCmd.print(F("#ERROR: ADC.SET_AVG requires 1 unsigned integer argument 'averages'\n"));
    }
    else{
        averages = atoi(arg);
        //set the param depending on the config mode
        if (adc_config_mode == ADC_CONFIG_CHAN0 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc0->setAveraging(averages);
        }
        if (adc_config_mode == ADC_CONFIG_CHAN1 || adc_config_mode == ADC_CONFIG_SYNC){
            adc->adc1->setAveraging(averages);
        }
    }
}


void ADC_START_sCmd_action_handler(SerialCommand this_sCmd){
    //increase CPU speed
    set_arm_clock(600000000); //600MHz
    adc_num_active = 2; //TODO allow for configuring channels 0, 1 or both
    adc_dma_start_continuous();
}

void ADC_STOP_sCmd_action_handler(SerialCommand this_sCmd){
    int chan;
    char *arg = this_sCmd.next();
    if (arg == NULL){
        adc->adc0->stopContinuous();
        adc->adc1->stopContinuous();
        adc_num_active = 0;
    }
    else{
        chan = atoi(arg);
        if(chan == 0){
            adc->adc0->stopContinuous();
            adc_num_active = (adc_num_active > 0) ? adc_num_active - 1: 0;
        } else if (chan == 1){
            adc->adc1->stopContinuous();
            adc_num_active = (adc_num_active > 0) ? adc_num_active - 1: 0;
        } else {
            this_sCmd.print(F("#ERROR: ADC.STOP must have no args (stop all) or specify channel '0' or '1'!\n"));
        }
    }
}


void ADC_RESET_sCmd_action_handler(SerialCommand this_sCmd){
    //ADC_RELEASE_sCmd_action_handler(this_sCmd);
    //restore our defaults
    adc_set_defaults();
    //restore the library defaults
    delete adc;
    adc = new ADC(); // adc object
}


#if defined(__IMXRT1052__) || defined(__IMXRT1062__) // Teensy 4.x
void TEMPMON_GET_TEMP_sCmd_query_handler(SerialCommand this_sCmd){
    this_sCmd.println(tempmonGetTemp());
}
#else
void TEMPMON_GET_TEMP_sCmd_query_handler(SerialCommand this_sCmd){
    this_sCmd.println(F("ERROR, NO TEMPMON for this MCU!"));
}
#endif


//------------------------------------------------------------------------------
// Unrecognized command
void UNRECOGNIZED_sCmd_default_handler(SerialCommand this_sCmd)
{
    SerialCommand::CommandInfo command = this_sCmd.getCurrentCommand();
    this_sCmd.print(F("#ERROR: command '"));
    this_sCmd.print(command.name);
    this_sCmd.print(F("' not recognized ###\n"));
}

////////////////////////////////////////////////////////////////////////////////
#else // make sure the example can run for any boards (automated testing)
void setup() {}
void loop() {}

#endif // ADC_USE_DMA
////////////////////////////////////////////////////////////////////////////////adc_num_active