import serial
from collections import OrderedDict
import numpy as np

#import IPython;IPython.embed() # USE ME FOR DEBUGGING

################################################################################
PORT = '/dev/ttyACM0'
BAUD = 115200
DEBUG = True

class ScmdComm:
    def __init__(self, port=PORT):
        self.ser = serial.Serial(PORT,BAUD,timeout=1)
    
    def send(self, cmd, endl='\n'):
        if DEBUG:
            print(f"-> {cmd}")
        self.ser.write(bytes(cmd+endl,'utf8'))


################################################################################
CHAN0_PIN = 2
CHAN1_PIN = 3
BUFFER = 1000
NUM = 1
RES = 16
RATE = 10
CONV_SPEED = 4
SAMP_SPEED = 0

class ADC_Driver(ScmdComm):
    def config(self,
               chan0_pin = CHAN0_PIN,
               chan1_pin = CHAN1_PIN,
               buffer_size = BUFFER,
               num_groups  = NUM,
               group_rate  = RATE,
               resolution = RES,
               conv_speed = CONV_SPEED,
               samp_speed = SAMP_SPEED,
              ):
        self.send(f"ADC.SET_READ_PIN {chan0_pin} 0")
        self.send(f"ADC.SET_READ_PIN {chan1_pin} 1")
        self.send(f"ADC.SET_BUFFER {buffer_size}")
        self.send(f"ADC.SET_RES {resolution}")
        self.send(f"ADC.SET_CONV_SPEED {conv_speed}")
        self.send(f"ADC.SET_SAMP_SPEED {samp_speed}")
        self.send(f"ADC.SET_NUM  {num_groups}")
        self.send(f"ADC.SET_RATE {group_rate}")
        
    def acquire_groups(self, num=1, **kwargs):
        kwargs['num_groups'] = int(num)
        self.config(**kwargs)
        self.send("ADC.START")
        groups = []
        i = 0
        while i < num:
            line = self.ser.readline()
            print(line)
            if not line: #FIXME empty line at end?
                continue #skip this iteration
            chan, index, t_start, V0, V1, t_dur = line.split(b':')
            V0 = np.array(list(map(int,V0.split(b','))))
            V1 = np.array(list(map(int,V1.split(b','))))
            X_micros  = float(t_dur)*np.arange(len(V0))/len(V0)
            data = OrderedDict()
            data['chan']     = chan
            data['t_start']  = int(t_start)
            data['X_micros'] = X_micros
            data['V0']       = V0
            data['V1']       = V1
            groups.append(data)
            i += 1
        return groups
################################################################################

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    OUTFILE_NAME = 'out.csv'
    OUTFILE_COLS = ['X_micros','V0','V1']
    OUTFILE_FMT  = '%0.3f,%i,%i'
    
    ADC = ADC_Driver()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #the following acquistion should take ~1 seconds
    groups = ADC.acquire_groups(10, group_rate=10,buffer_size=1024)
    #restructure data for plotting
    for g in groups:
      V0 = g['V0']
      ax.plot(V0,'.-')
    ax.set_xlabel("Time [s]")
    ax.set_title(f"ADC-12bit test")
    plt.show()
    
